
[哟西，感觉不错呢]

确认到複数的双足飞龙变成冰镇之後，我放下了剑。

勇者时代每天都持续挥动的【勇者の剣】也久违了，感觉不怎麼好。

通过马尔库特（マルクト）加护使得让『精霊』工作变成可能的剑，是比任何人都值得信赖的伙伴。
嘛啊，这之後的人生，力量不是必要的，因此也没什麼机会可以使用吧。
关於力量的事，没有放在心上的必要吧。

[做的不错，勇者哟]
[喔。魔王才是，辛苦了]

如此说着，与转移到这边的魔王击掌了。
虽然是初次的共同作业，但一切顺利，就互相称讚对方战鬥了。

[我与勇者果然步调很合呢！]
[是啊。我们已经是合二为一了那样呢]
[……会把那話当做是工口意思来听，我也欲求不满了麼。呀咧呀咧]

不对不对，呀咧呀咧什麼的可是这边的台词哦。

[那麼，难得捕获到新鲜的……怎样都想美味地吃呐]

看了冰镇的双足飞龙后，魔王也若有所得地大大的点头了。

[对了，叫『五帝』吧。反正那些傢伙应该也在汏拓（ダァト）的，这样也不错呢]

说这些，好像是在这边想到了什麼呢。
五帝么……要呼叫那群傢伙么。

太吵了所以不怎麼想让其过来，不过魔王如此希望就没办法了。
而且，虽然是群脑子奇怪的傢伙……不过听过其料理的手腕是真实的传闻。


[明白了。就交给那群傢伙吧]

俺点头也同意了後，魔王马上就呼叫他们出来。

「【召喚】――『五帝』」


行使的是，魔王的固有魔法【召喚】魔法。
使用了这个可以呼叫出契约的对方的魔法，把在汏拓的五帝叫出来了。

[……哦，来了呢]

接着，从魔王显现的魔法阵那——五名魔族出现了。

应该是突然召集的，但是他们简直像是等待着一般，无言的仁王立。

接着，开始叫喊着意义不明的話语。
一直以来的自我介绍，开始了。

[將你的心房斬開，剑帝『索德曼（ソードマン）』参上！]
[我會燃起你的激情喔,炎帝『肺热曼（ファイヤマン）』参上!]
[好想融化在你的爱裡面，粘帝『史莱姆曼（スライムマン）』参上！]
[可以把你吃掉嗎?食帝『塔贝尔曼(タベルマン）』参上]
[料理你的愛慕。料帝『刻苦曼（コックマン）』参上]

『五人合起来就是，五帝！在此参见！！』

PA PANG!!

如此这般，帅气地出场是不错，但是太长太吵了，让我和魔王都皱眉了。

即便如此，他们都没有在意。

[呜呼！敬爱的魔王大人哟……有何吩咐]

演戏般的态度，对着魔王跪下了。

[……如果不是这样的話，你们的评价会更好地吧]

魔王苦恼地抱着头。
实力是有的……性格却是灾难，但是魔界是势力主义的，因此什麼都说不了。

他们是魔王君持有称号的一员。四天王之下『五帝』的各位。
是称作剑帝，炎帝，粘帝，食帝，料帝的成员所组成的他们，
是魔界的料理调达，兼任魔王城的料理厨师。


[嘛行了。刚才，我和勇者捕获了双足飞龙……简单的就行了，适当地弄成好吃的过来]

魔王也认同了其实力与料理手腕的样子，因此才叫到这个地方吧。


『交给我把，我所爱之人哟！』

受到魔王指示後，五帝用演戏般地动作行了一礼後，马上就开始行动了。
靠近了被冰镇的双足飞龙中的一只。
料理开始了。


[索德曼，要斩了！]

首先是双手持剑的剑帝索德曼，把冻住的双足飞龙斩碎——嘚，喂。
胡乱地斩碎，那个手法不是料理。只是单纯的在斩啊。
最终完成的，是冻着的小小的立方体，那是双足飞龙的残骸。

[肺热曼，要燃了！]
但是料理还没停下。
全身像火焰般摇曳着，炎帝肺热曼把立方体放在火裡。
冰融化了，接着肉燃起来了。
虽然已经焦了……

[史莱姆曼，要黏糊糊了！]
这麼说着，在这迷之工程上又加了一个。
像史莱姆那样黏糊糊的，粘帝的史莱姆曼给烧焦的双足飞龙加上了粘液。


自此已不再是食物了。

[塔贝尔曼，要吃啦！]
接着最终工程。
脸部是嘴巴的新颖生物的塔贝尔曼，把食物变成已经无法形容的什麼东西一口吃下。
接着露出了满脸笑容，说了一句話。

[超好次]

——哈？
不由得想打一顿了，但总算是忍住，把脸转向魔王。

[这个流程是必要的？]

真的是意义不明啊。

[对这些傢伙来说这是必要的吧。不要追究了]

魔王好像没有吧这个闹剧放在眼裏。
打了个大大的哈欠。

喂喂，这样的話经过多久都不会做出料理的吧……虽然这麼想到。

[刻苦曼，料理完成了！]

头戴白色厨师高帽的料帝的刻苦曼，把看起来很好吃的烤双足飞龙端上来。

看起来料理结束了呢……

[呐啊，前面的四个人是必要的么？]

不管怎麼看都只能看出在胡闹啊。

[不知道。比起那个，快点吃吧！啊，五帝回到原来的工作去就行了。赶紧地回去]


对着看起来很美味的排骨眼睛发亮的魔王，嘘嘘的赶走五帝。


『那麼，在星星相遇的命运之时再会！』

五帝直到最後都是我行我素，离开了这个地方。

因为战鬥过，知道实力是确实的，不过……那样真的没问题么。

嘛，不管了。
总之，眼前有着看起来很美味的肉在。

现在就只集中在这件事上吧。


[我不客气了]

魔王首先先张嘴咬了一口。

[——！？美味喵]

原来如此。看来轻咬都能感觉到美味呐。
哪裡哪裡？我也从魔王的对面咬下一口。

[——！？好吃，呐……!]

比想象中双足飞龙的肉更加的好吃。

虽然肉很厚，但是咬下去後在嘴裡没有抵抗地马上就融化了，上等的肉香没有腥味，让人无話可说呢。

这样的話无论多少都能吃下去！这是让人这麼想的肉呐。

[…………！！]

两人一段时间内都没有说話，持续的吃着料帝準备的大量的肉。

最终在肚子饱了后相互对视，无论那边都是肚子鼓鼓的。
性格虽然是那样……五帝的——不对，不得不承认料帝的料帝是美味的呐。

[很美味呢，勇者哟！]
[虽然有点遗憾……不过满足了。嗯，吃到了美味的东西，而且与魔王一起很开心哟，谢谢]
[那，那样我也是一样的哦……真的很开心]

饭後。我们躺着，交换話语。
与魔王对話，非常地愉快。

虽然在一起只有一个月左右……我觉得大概是融洽的。

[————啊]

突然注意到了。

现在的我，是幸福的……呢。
什麼都不必担憂，烦恼也没有，只是在她的身边笑着的现状，真的是无与伦比地幸福呐。

但愿，这之後也。
能拥有与她一起幸福满溢的日常——

如此这般的，看着魔王的笑容祈愿着。


[嗯？怎麼了，勇者啊。默默地在笑着呢]
[呀，没有啦……什麼都没有哦。只是，觉得幸福什麼的而已]
[是么！那样的話，我也幸福哦]

嗯，谢谢，魔王。
托你的福，俺……终於，能变得幸福起来了呢。


…………完…………

