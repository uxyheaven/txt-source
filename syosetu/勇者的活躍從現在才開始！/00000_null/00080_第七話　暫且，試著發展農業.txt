「刚开始就不坏啊。」
「收获的季节啊！虽说和季节没有关係。」

种庄稼的一个月後，我们的村子………因为只有三个人，所以準确来说并不能叫村子………迎来了收获的季节。
偶尔我们也会帮忙，魔法人偶们照顾的作物开始收获，他们正在努力收割着。
收获的农作物在筛选过形态和质量後储存在仓库的地下。
这一个月期间，在放置魔法人偶的仓库底下设置了鍊金仓库。
放入这裡的食物，无论经过多长时间，都会很新鲜。
我用的『收纳』魔法也有同样的效果，这也是鍊金术之一。
因为对面的世界常常会食物不足，所以一切的浪费都是不允许的。因此某种程度上，有一定规模的城镇一定会设置这种鍊金仓库。
因为食物放入这裡不会腐败，所以不会有浪费食物的情况。
虽然是很便利的鍊金工具，但也并不是万能的。
在被魔族攻城时，被设置的鍊金仓库成为了居民的命脉。这些有限的食物，谁能吃到？
贵族，神官，富翁们都会優先考虑自己与家族，这样就会因为捨弃了處於末端的居民，而造成饥饿的居民叛乱。
那时虽然能保障一点食物，但居民马上就会被街上围着的魔族和他们所操控的魔物全歼了，还会有悲剧發生。
不过在这个世界，和这种担心是无缘了。
闲話到此为止。

「哥哥，药草怎麼样？」
「下次採摘时提高一下质量。」
「不行吗？」
「质量有些差，不过可以正常使用。」

現在這藥草呢，似乎不是最高質量呢，看來是因為土壤提供的養分太少。
大概等同於中階治療藥劑吧，不過因為過程不怎麼辛苦，所以這樣就夠了。

「咕伊－－－－！」
「漠漠也辛苦了，比想像中更加出了不少力呢。”
「咕伊－－－－！」

與安莉两人确认草药的质量後，发现了漠漠。
那个嘴裡叼着一只大野猪。
不要看漠漠虽然是小型化的样子，但也是可以轻易猎到鹿和猪。
在这附近，不，在这世界能打败漠漠的生物是不存在的。

「多亏了漠漠，让这附近的鹿和猪都变少了。真是太好了，非常感谢。」
「咕伊－－－－！」

田地裡开始生长农作物，以此为目标的猪、鹿、狐狸、果子狸等也开始增加。
虽然野生动物驅逐的警衛是魔法人偶的工作，但可能遭到破坏。
因此，漠漠开始驱逐了。
狩猎对漠漠来说也算是发泄压力，而捕获到的猎物，不只肉也吃，而且不能使用的部分就发酵堆肥。

「漠漠，不能像这样吃猪呢」
「咕伊－－－－！」
「烹调了比较好吃。」
「咕伊－－－－！」

对漠漠来说，饮食是兴趣的一环。
所以，狩猎猎物後就没吃。
对被烹调了的鹿和猪肉的肉好象很中意，立刻就把血和内臟带到这边来。
安莉能做各种各样的料理。

「今天，就吃野猪火锅吧。」
「咕伊－－－－！」
「用味噌醃漬後再燒烤的最棒了！」
「了解。」

因为安莉做料理很好，所以也能学习到这个世界的料理。
另一方面，我完全没有做料理的才华。
从掌握了鍊金术来看，也并不是笨手笨脚的，却不知为何不擅长。
只让安莉做，也很难过。不过，如果我做，漠漠不会有好脸色的。
因为漠漠是美食家，所以她说我的菜做得很糟，就不吃了。
幸好，安莉好像很喜欢做菜，我就完全交给她了。
在她做饭的时候，我会做其它家务，也很有效率。

「收获结束了，接下来就将那播种。」
「是啊，因为魔素石太可惜了。」

魔力的效果最显著的，就是使农作物早熟。
我對這個世界原有的農業沒怎麼留意過，不過世界上能一個月就有收穫的農作物其實不多呢。
或許寒冬的天氣成長期會變緩，如果不在意的話，那麼一年可以收穫８～１０回吧。
做那样的食品怎什麼疑问也没有，不过，魔素石一次次地填埋，培育农作物也需放出魔力，魔素石一次次变小了。
原本空曠的農田有著大量的雜草叢生關係，所以不管什麼作物都試著摘培看看。
在那裡的世界，即使把所有的土地都充分利用也食物不足，所以这个世界受惠了吗？

「药草的比例会增加吗？」
「如果只是药物加工的話，光是鍊金儲藏庫裡面的量就要忙個不停了呢。」
「是啊，想要做各种的药。」
「那蔬菜怎麼办？就算加工也不会让体积减多少哦。」
「把蔬菜卖了也是一种手段。」

好不容易制作的蔬菜，会从别人那裡得到怎样的评价呢？
我变得十分在意，决定立刻把蔬菜卖了。

「自由市场吗？自己和这边的人很熟悉。这裡和王都的市场很像。」

新买的小货车因为要堆积蔬菜和魔法人偶，直接行驶并且往返需要六小时，所以小货车只能用来“收纳”，乘坐漠漠的話五分钟就能到城市的郊外。
不过漠漠的速度依然很惊人。
另外，龙的飞行属於魔法的範畴。
所以即使速度超过马赫的速度飞，也不会产生冲击波。
用魔法使自己的样子消失，就可以在全世界範围内任意移动了。

「哥哥，真想去海外旅行啊！」

安莉的兴趣是从网上获取世界文化和娱乐的信息。
不久前也从旅行社的主页上看到新婚旅行的相关信息。

「再冷静一下吧！」
「是这样啊，很期待呢。」

但是，安莉是没有护照的。
果然，只能乘坐漠漠飞走啊。

「哥哥，到会场上了。」

漠漠在镇的郊外没有人的地方着陆，在漠漠着陆的地方放出被我收纳起來的小货车。
三个人乘上，剩下的就只是向自由市场的会场出發了。

「漠漠，保持安静」
「咕伊－－－－！」

今天漠漠也是在安莉背负的背包裡，而且还要进行新的作战。

「我是御影，我先前有申请過販賣許可。」
「御影先生，請來这边。」

在向自由市场的工作人员打完招呼之後，得到了一个销售蔬菜的空间。
在那边放置小货车，把拿来的蔬菜陈列好并标上价格。

「哥哥，先不管賣得好不好，主要是能賣光就很開心了。」
「也是啊……」

因为有其他贩卖蔬菜的农民，所以我们的价格也与他们的一样。
在异世界很特殊的蔬菜，萝蔔，西红柿，茄子，青椒，葱，卷心菜，秋葵，黄瓜，这个世界上也只是普通的蔬菜而已。

「安莉，咱们的与他们的哪方看起来更好吃？」
「我觉得都一样。」

使用魔力与魔素石种植的蔬菜，外观与专业种植的一样。
味道也很美味，所以还是会有客人光顾吧。

「欢迎光临，完全没有使用农药和化肥哦。」

肥料用的是自然堆肥和魔素石，农药也是不让虫靠近的药草，只使用了原料的忌避剂。
除草剂，则是由魔法人偶们勤勉除草，所以没有使用。
这就是一个好的宣传就好了。

「试着卖就好了。哥哥毕竟是新人嘛。」
「在深山裡开始发展的农业了。」
「这样啊，加油吧！」

老爷爷，老奶奶，阿姨等都买了一些菜，不过还是餘下了三分之一。
以味道为宣传，并且最後又以便宜的价格卖出，结果终於是卖完了。

「做生意的道路是崎岖的。」
「但是，这可是相当的销售额呢。」

因为幾乎不需要成本，所以销售额幾乎皆为纯利润。
所以说结果并不坏嘛。

「但是，销售就这样结束了。」

自由市场是不定期举办的，农协主办的早市，也没能参加。
因为我们没有从农协处买农药和农机具。
我不打算去农协，所以当然不可能可以去销售。

「农协，是类似于行会的东西吗？」
「是怎样的？那裡严格吗？」

在对面的世界，行会的力量是非常强大的。
如果随便做生意的話，就会在晚上被杀掉，尸体置於市区之外，被魔族吃掉，连骨头都不会剩下。
魔族的侵略，慢慢的商业圈和销售额减少了，被赶出住处。那个时候失去财产的商人，连行会的会费也不支付裡便开始做生意，而且被生活所迫，他们只能薄利售货了。
行会所属的正规的商人们，他们自己也会倒下，让背地裏做生意的傢伙们当作非法的人处分了，但是因魔族而穷困的商人幕後生意反而不可能停止，所以这种情况不断增加。
反过来背地裏做生意的傢伙相互勾结，正规行会会员被袭击了，我本能地理解做生意的道路是血淋淋的。
与此相比，农协则不会剥夺违反者的生命。

「賣得真是畅销啊！以後也能继续賣吗？」
「每個禮拜會來一次，會用小貨車載來這個這個地方來兜售。」

实际上，鍊金仓库裡，还有相当的份量的蔬菜。
魔法人偶们不去玩而又开始栽培了的也有，也有去把库存调整好的，为了我们不被怀疑，挣到能够生活的钱就好了。

「哥哥，你有什麼指望吗？」
「正在试着寻找。」

因为所有蔬菜都卖了，我们在街上买了些东西回家。
然而销售额還不坏，那我们作为一个小规模的农民又向前迈进了一步。

