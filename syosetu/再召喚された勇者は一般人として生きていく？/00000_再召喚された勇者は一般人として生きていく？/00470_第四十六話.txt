
通过检问後，三人搭乘了马车前往溪谷。

溪谷有着可以容下幾台马车并排着也没问题的程度的宽阔。
耸立在左右的岸壁的上面有着足以让人通过的洞穴，在那裡也配置着精灵的卫兵。
精灵族就有如各种故事中传承的那样大多都是厲害的弓手，即便强行突破了检问也会被来自上空的箭给封住行动吧。

坐在车夫座的苍太一边听着坐在後面的两人的对話一边抓住缰绳。

「話说回来师傅。在我们被带到班房的时候一下子就来了呐，当时你是在哪裡呢？」
「我也是为了出去寻找雅蕾洁露才来的，但被卫兵阻止了呐。没办法只好待在入国管理所的事务所。在你们被带去班房的期间受到联络而赶过去的」
「嗼，师傅出去搜索实在是太乱来了啦！」

雅蕾洁露用生气的表情对奈尔亚斯叮咛道。

「「轮不到你来说」」

苍太突然说出的話，碰巧地奈尔亚斯也同时说道。

「噫，对不起！！」

站在上边的精灵们向奈尔亚斯和雅蕾洁露和蔼和亲的样子送去了微笑的视线。
然而，对於带着那两位的人族的苍太则送出了包含着疑念·厌恶·生气之类的负面感情的视线。
苍太虽然察觉到被看着，但依然丝毫不介意地一边哼着歌一边取出水果吃。
那副姿态让精灵们感觉仿佛被逆抚了感情一样。但是，因为乘坐在马车裡的是贵重的人而忍住怒气，收起箭矢。

「苍太先生，你在吃着什麼吗？」

雅蕾洁露并未察觉那种视线，反而是在意着苍太正在吃的东西。

「是在从朵拉出发前大量购买的水果。可以的話要吃吗？反正还有着数几桶呢」

转身向後，将取出的水果拿给她们看後，两人都探出身子。
似乎在精灵之国是很少见的样子，两人都是兴趣十足的表情。

「我就不客气了！因为苍太先生準备的食物是不会有错的呢！」
「嚯，那还真是有趣呢。我也能拿一个吗？」

苍太在取出後就往後抛去。
奈尔亚斯完美地接住了，雅蕾洁露则打到了脸。

「好痛，呜，没能好好接住」

看了那个的苍太摆歪了头。

「因为有着能跑到外面去採取素材的行动力，所以还以为你的运动神经很好呢，看来是误会呐」
「就只有行动力才是唯一能看的优点呢……其他的都无法跟上的样子呐」
「啊，我知道。的确是那样的感觉」

雅蕾洁露交互地看了嗯嗯地互相点头着的两人後，鼓起脸颊并表示出生气的样子。

「嗼，两人都好过分！我也是很正常的喔！」
「「……」」
「呼，请不要两人都一同沉默不语啊！」

看着那样的雅蕾洁露，两人都露出了笑容。

「嗼！比起那个师傅快吃吧，毕竟难得拿到了……真好吃！！」

为了混淆話题而吃了一口後，先前的怒气都不见并绽放了笑容。

「有那麼好吃吗，那麼我也……的确是很好吃呢」

两人连接不断地吃着，不到五分钟就吃完了。

「记得，还有很多吧？能再来一个吗？」
「雅蕾洁露，很狡猾喔。苍太殿下，我也能再来一个吗？」

苍太对於两人的吃相感到吃惊的同时从背包裡再度取出并交给两人。

哼哼。
在说着那样的对話的时候，埃德为了告知苍太什麼而如此叫了一声。

「怎麼了？埃德」

将视线移回前方後，看到了溪谷的终点，在那更遥远的前方有着一颗巨大的树。

「苍太殿下，那是精灵王国的守护神『神圣之树』。是以紧靠着神圣之树的方式整备着国家的」

虽然奈尔亚斯在为吃惊的苍太和埃德说明，但苍太却是在另一个意义上的感到惊讶。

「已经变得那样大了吗，能确实地感觉到时间的流逝呐……」
「欸？苍太先生有什麼事吗？」

雅蕾洁露在听到苍太的低声细语後提问了苍太，不过苍太却摇摇头并回答说没什麼。

在穿过溪谷後是个缓缓向下的坂道，在下去後就会到一个广阔的森林。
和苍太与雅蕾洁露相遇时的那个阴森的森林不一样，阳光适度地照了下来并且也感知得到生物的气息而使其很温暖。

「果然森林就应该是这样的呐」
「嘛，你想说的事我也能理解。毕竟那个森林的确很凄惨的呢」

两人回想起黑暗之森而使得表情变得阴沉。

「凄惨的森林是指怎样的森林呐？」
「我和雅蕾洁露相遇的那座森林啊。是位於比那个检问还要往东的森林，暗到让人以为是晚上也不奇怪的地步」
「雅蕾洁露，那真是你一个人过去採集的森林吗？」

奈尔亚斯因为苍太的話语而询问了雅蕾洁露。

「呜，十分抱歉。没错，就是那座森林。虽然是有和平时不一样的奇怪的氛围的感觉呐」

雅蕾洁露带着好像在道歉一样的脸回答道。

「是吗。之前去的时候还是普通的森林呢……或许發生了什麼呢」
「嗯，那座森林的确是很奇怪。首先就是很暗，太暗了。就算是白天也一样。再来就是全体来说魔素很浓，虽说是那样但魔素并不是停滞着的样子」
「嗯，根據情况的話有必要去调查一下，吗」

在听了苍太的说明後，奈尔亚斯露出了深刻的表情并深思着。

森林之中的道路被整备得很好，也因此没有高低差而使得埃德的脚程变得轻快，过不了多久就穿过森林了。
那座森林的终点以宛如在绘画着圆形的方式包围着首都。
到达了精灵王国的首都後，其被城墙保卫着。

首都就像一个城下町，然後在中央的神圣之树的侧边则建立着一座城堡。
在到达东门之後，只有苍太被要求要进行以往的检查身份证和通过水晶的犯罪履历检查。
奈尔亚斯和雅蕾洁露则仅靠脸就被许可入场了。

结束了入场检查，再度乘上马车并进到城市後奈尔亚斯向苍太搭話道。

「苍太先生，欢迎来到精灵王国的首都『基爾诺尔（ギルノール）』！」

-完-

