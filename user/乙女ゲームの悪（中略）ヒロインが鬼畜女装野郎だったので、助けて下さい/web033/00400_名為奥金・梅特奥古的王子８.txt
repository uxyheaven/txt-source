
　至于此世，更是与恋爱无缘。对阿鲁库大概只限于模糊的憧憬吧。
　…也就是说至少17年来，我与爱情之流可谓无缘。
　自然无法感同身受。

『你们可知道爱情是怎麼一回事？』

　我的脑中忽然浮现出这句話。啊咧，这是什麼台词？
　…对了，记得是『费加罗的婚礼』。歌剧『费加罗的婚礼』中，女主角所唱的歌的名字。①
　虽然我既没有看过歌剧，也未曾听过歌曲，但很久之前读到过音乐课本上的记载，记得当时非常喜欢这句短语。

　你们可知道爱情是怎麼一回事？

　当时尚是个纯真少女的我，对这句話产生了强烈的共鸣。不知能否称之为恋情的，青春期特有的模糊悸动。
　清晰可辨的「爱情」肯定、肯定会在某日造访，那个时候我如此坚信着。
　我以为，自己总有一天会成为大人，仿若戏剧与漫画般，陷入再清楚不过的、激烈又热情的爱情。我毫无根據的以为，自己会犹如故事中的女主角般，终有一日与帅气的异性坠入恋河，获得幸福。
　然而，现实对这份纯真毫不怜惜。该说可悲吗，一直同激烈的爱情无缘的我就此在丧女道上狂突猛进。…嗯，前世少女时期的我啊，容我再次道歉!!

　你们可知道爱情是怎麼一回事？

　我本以为总有一天会得到这个问题的答案，然而身叠与前世相加共计46年岁月的现在，我仍不清楚。…毋宁说随着年月增长越发不懂了。

「…那副样子，看来你似乎没体验过呢。」

　这麼说着的奥金的脸上。…也许是心理作用，貌似带着炫耀式的表情。你在得意些什麼啊？
　身为不了解爱情的丧女真是抱歉了啊!!体验过爱情就那麼伟大么！…話说，虽然在你得意洋洋的时候这麼说有点对不起，不过就算你了解爱情，最後也无法如愿吧。完全不是能断言现充（嘛，从不缺女人倒贴的时候起就已经完全是个现充了…爆炸吧）的状态吧——!!单相思才不是那麼值得趾高气扬的事啊喂。

「…那又怎样？就算不知晓爱情为何，也完全没问题呢。」

　我挑起一边眉毛，用扇子掩藏起嘴角。
　那张得意洋洋的脸实在惹人生气，虐虐他好了，就这麼办吧。

「反正，对我们这样身份高贵的人来说，基本都是政治婚姻。关键在于能否为家族所用，仅此而已。没有必要困於爱情。」

　这个世界一方面提倡着自由恋爱，但另一方面仍旧残留着结婚当从父母意愿的意识。身份地位越高，这份意识就越强烈。
　不论哪个少女都憧憬着灰姑娘的故事。出身庶民的少女为王子一见倾心，成为公主，走向幸福。
　这样的事情事情在现实中很少见，即便真的出现，结婚之後等待着的将是非同寻常的磨难。
　遊戏就只讲述到奥金和安洁同居为止，但实际同居之後还会有接二连三的波折吧。自由总是伴随着代价的，自由恋爱亦然同理。
　与双亲选定的政治婚姻的对象，一同努力成为优秀的夫妻。恋爱不过遊戏，是结婚前短暂的迷梦。这才是一般贵族的正确态度，我也是如此打算的。毕竟这条路的轻鬆显而易见。
　所以，对遊戏抱有难以割捨的恋爱情感，与身为贵族紧抱麻烦同义。绝非能骄傲宣告的事。

「——嗯，我也是这麼想的呢。」

　这麼说着，奥金露出苦笑。…明明想虐虐他的，反应却如此平淡。真是个无趣的男人。

「不能拥有爱情，作为梅特奥古王家的直系血脉，理应迎娶门当户对的对象…比方说，露库蕾娅小姐这样的人，我是这麼想的。」

「…这种地方提到我的名字，实在令人诚惶诚恐，就我个人而言这种話容我谢绝呢。就算只是假设比喻。」

　尽管知道这有些无礼，我还是难忍嘴角的抽搐。
　…在魑魅魍魉横行的王宫裡成为王妃啥的!!绝对不要!!
　光是想象我都战慄不已。

「……可是呢，那样的想法，一瞬间就消失不见了。」

　这麼说着，奥金望向不知名的远方，眯起眼睛。

「和她相遇的瞬间，这样的想法就全都被我丢到天外了。」

　然後奥金，一边思念着不在此地的某人，一边露出心荡神驰的甜蜜笑容。
　不论谁见到都能确信他正处在热恋之中的，甜蜜的甜蜜的笑容。


译注①：歌剧『フィガロの結婚』，中译《费加罗的婚礼》，是莫扎特最傑出的三部歌剧中的一部喜歌剧。『恋とはどんなものかしら』（你们可知道什麼是爱情）出自剧中第二幕，是男仆凯鲁比诺为伯爵夫人的献唱，只是剧中男仆由女中音扮演。这首歌是男仆为苏珊娜所写并唱给伯爵夫人的，作者写的女主角唱啥的，大概是搞错了。
