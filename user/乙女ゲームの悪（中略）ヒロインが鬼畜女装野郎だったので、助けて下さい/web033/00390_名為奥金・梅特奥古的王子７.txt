
　我说的話，令奥金惊讶的眨着眼，长长的睫毛啪嗒啪嗒晃动。

「流传着这样的流言吗？」

「…虽说仅限于一部分人之间。」

　骗人的。
　完全是虚张声势。
　然而，以全然看不出这是空挥的模样，我向奥金露出挑衅的微笑。

「恋爱这种东西真是麻烦呢。因为就算殿下再如何擅长八面玲珑，也没办法完全遮掩起来。虽说不清楚另一位是谁，不过殿下陷入了恋爱这件事，一看到平时殿下送去的那炽热的目光就一目了然了呢。」

　热恋着被认为是下届王的奥金的女学生为数不少。我也曾听说存在有大规模的粉丝团体。若是情报源来自粉丝团体中的某个人的話，能根據奥金的举止揣摩出他内心隐藏的想法实属正常。
　…嘛，其实不过是套話而已。收集情报是很麻烦的事情，我并没有去特意收集过。擅自行动的話，作为提供情报的代价，不知道会被提出怎样的要求。
　情报屋也是，不可能全都是琪艾菈那样既有着浅显易懂的报酬规定，又有信誉的人。还存在本应恪守保密义务的委托内容被作为情报转卖给其他人的可能性。说实在的，身为大贵族伯雷亚家的一员的我不想被人利用。另一方面，对於是否使用伯雷亚家专属的谍报员我也十分犹豫。
　反正我拥有乙女遊戏玩家的记忆，不必获得确凿的证据就能做出推测。能不付出劳力当然是再好不过了。

「殿下对谁一见钟情了，根據事态发展伯雷亚家的立场也会随之变化。因此，我想来确认流言的真相。…对於尾随跟踪这样的举动，我表示歉意。」

「不…那个…真是败给你了。」

　奥金深深叹了口气，抱住脑袋。银色的头髮摇曳着，反射出绮丽的光。…哎呀，真是飒爽的符合王子气质的头髮呢，真心的。

「明明还以为完全隐瞒住了的…」

　完美，套話成功!!真不愧是如此高规格的我!!完全骗倒了王子哦!!
　…但是，即便面对的是我这样演技力超群的完美美少女，如此轻易就承认没关係吗。让这种傢伙当上王真的没问题吗，这个国家。怕是外交上被人下了绊子也察觉不到喔，真是的。

「…不能小看恋爱中的少女的洞察力呢。」

　我掩藏起内心的想法，展现出游刃有余的笑容。保持冷静最为重要，无时无刻都要表现出冷静的姿态。

「确实…思春期的女性太难应付了，真是麻烦的生物啊。」

「啊啦，只提女性什麼的真是过分呢。思春期的男性，就不会發生这样的变化吗？陷入恋爱之人的变化，是不会因性别不同而不同的。因为恋情，是种会让人时而聪明、时而愚蠢，陷入反覆无常的变化之中的东西。」

「…没错。这点毫无疑问，这麼一想的話，爱情真是种非常可怕的感情呢。」

　奥金理解的点了点头，比刚才更为用力地叹了口气。
　然後他直直地注视向我，询问道。

「…呐，露库蕾娅小姐，你恋爱过吗？」

「——我、吗？」

「只要想到那个人就会胸口发痛，尽管如此，脑子裡却一天到晚除了那个人其他什麼都装不下，宛如发烧般的激烈爱恋。」

　奥金的話，令我皱起眉头陷入沉思。

　…究竟如何，我是否恋爱过呢。

　前世，大概算是有过恋爱的吧。虽然丧女属性大为活跃，但好歹活了29年，多多少少有过些恋爱经歷。
　然而，再如何翻检记忆，我也不记得自己曾陷入如此激烈的恋爱过。
　认为自己喜欢着某人，感觉上不过是这种程度的情感而已。开始相亲之後，相比内心的悸动，我反倒一心着眼於现实条件的优秀与否呢…。
　…不对，話说，二次元角色会定期填满我的梦境呢。一天到晚净是想着那个角色的事，胸口仿佛被勒紧般，为了填补这份永远无法实现的恋情，我到处搜集角色周边…。唔嗯，这状况和奥金所说的話简直一般无二，恰好对的上…。
　…啊咧，这是恋爱？萌上二次元，恋爱？好奇怪啊，完全搞不明白了。
