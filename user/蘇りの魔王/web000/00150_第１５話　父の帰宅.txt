父亲回家，是自那以後又过了两周的事了。
伴随着渐渐高升至正午的太阳，乘坐着马车从邻镇回来的父亲，既没有身穿铠甲，也没有佩剑，穿着一身就像是哪裡的村民或商人一样的轻装，乍一见根本看不出来是一位很有些手腕的剑士。
父亲给人的感觉温和而平静，行李中还带着几本厚厚的书籍。因为之前一直在路上看书吧，父亲正戴着他阅读文字时总会使用的视力矫正用魔道具。与其说是骑士，那个身姿倒不如更让人觉得是一位学者。
在村子的入口跳下马车，然後将那几本书放入带着的大包的父亲，向前来迎接的鲁鲁（ルル）慢慢地摇了摇手。

“呀……好久不见，鲁鲁。过得好吗？”

那是就像以前一样，与外表别无二致的温和语调，鲁鲁已经有幾年没有听到这个声音了，不得不说让人怀念。
“啊，啊……过得很好。这次回来能待长一点吗？”

父亲，帕多利克（パトリック）的工作地点，是王都的王宫中，因此不太能得到休假。由於明白这点，所以鲁鲁才会这麼问。
本来的話，一家人一起住到王都去比较好，但是母亲特别中意这个村子。所以，父亲把母亲和鲁鲁留下，没有带她们一起去王都。
母亲以前是住在王都的，但跟父亲结婚後，便来到了这个村子。
两人都并不喜欢王都。似乎是有着什麼原因，但他们不肯说，所以鲁鲁也不太清楚。结果，也许是因为那样的原因，鲁鲁也就无法前往一些大一点的城市了。
“是呢，这次也……不能待得太久呢。马上就要回去了。所以，只能陪鲁鲁和拉斯帝（ラスティ）训练一小时左右了。”

帕多利克笑道。
一如既往，帕多利克回来後，便会去指导鲁鲁和拉斯帝练习。
这个村子裡，大致有些剑术心得的人不在少数，但能施加以正确的剑术训练的人，除了帕多利克外，便再也没有了。
因此，当他像这样回到村子裡时，会有不少村子裡的孩子跑去向他请教。
待在村子的期间，帕多利克是處於完全休假中的。因为什麼工作也没有，所以很有空闲。
虽说如此，也不能让身为骑士的手腕荒废。因此，剑术的修行一天也不会中断。而其他的孩子在看到後，便开始央求他来对自己进行指导。
就这样受训于帕多利克，然後离开了村子成为冒险者的人也不在少数。拉斯帝就正以此为目标努力着。
“这次拉斯帝也燃起来了。大概，会相当拼命地努力吧……请一定注意不要让他太勉强了哦。”

“嗯？那个孩子發生了什麼吗？”

“那些以後再说吧……还有很多不得不说的事情。”

“是吗？……这麼说来，麦蒂娅（メディア）呢？”

“现在，和伊莉斯（イリス）一起做午饭。因为不知道马车什麼时候到，所以我才会等在这裡。”

这麼说来，嗯，帕多利克点了点头。
“伊莉斯吗，确实从信裡听说了。不过，要成为我家的养女是？因为细节什麼的都没写，所以到底是怎麼一回事……”

那是指母亲寄的信吧。
村子裡發生的事啦，家人的近况啦，母亲会定期将这些写成信再寄出去。
寄到王都明明最短也要花上一周左右的时间的，但这次却提前送到了。
尽管如此，看来也并没有把细节都写清楚的时间。
“本人倒是说成为侍女就可以的……但妈妈不同意。”

因为鲁鲁那已经理所当然地接受了一般的语气，帕多利克吃了一惊。
但看来也不是反对的样子，因此帕多利克向鲁鲁问道。
“麦蒂娅也真是够乱来的……嗯，鲁鲁觉得这样真的好吗？”

突然有外来者成为家人什麼的，不会觉得讨厌吗？大概就是类似意思的委婉询问。
因为在帕多利克看来，鲁鲁还是孩子。
所以，这是理所当然的关心。
但其实完全不是那样的。
伊莉斯是从以前开始，就像家人一样进行了交往的朋友的女兒。而鲁鲁自己也在前世建立了，即便是陌生人也能毫不在意地与其一同生活的大胆性格。
因此，完全没有问题。
“我完全没关係的哦。伊莉斯她是一个大方有礼的好孩子……”

帕多利克听完鲁鲁的回答後，从那个話语中感觉到了单纯的赞同以外的什麼东西，因此一边用手摸着下巴，一边两眼发光地说道。
“呵呵……好孩子吗？”

这句話中，多少掺杂了一些戏弄的成分。
就像是亲子间接触常做的那样。
但是，鲁鲁早已过了会因那种事情而动摇的年纪了，因此用非常吃惊的口吻说道。
“今後也只是单纯的妹妹而已啊？怎麼可能把她当作那种对象……”

说白了，伊莉斯是重要的友人的女兒。
对她出手什麼的，心理障碍太大了。
但是，帕多利克却提出了明确的论据来反驳。
“王国的法律，并没有禁止没有血缘关系的兄妹或姐弟通婚。那个情况之下，其中一方基本都是家中的养子啊。”

那是事实。
家中的王国法典中，确实是如此记载着的。
但是，问题不在那裡。
所以，鲁鲁答道。
“不是法律上的问题……”

然後，帕多利克大概理解了，鲁鲁对伊莉斯抱持着的，并不是恋爱之类的感情。
“嗯……该不会，虽然可能性很低，但真的把对方当成了妹妹吗……”

两人的谈話，就以帕多利克这麼一句奇怪的自语作结了。
然後，鲁鲁从帕多利克那收到了几本书，便先行向加蒂斯洛拉（カディスノーラ）邸走去。
帕多利克也紧跟着他。不久之後，两人便到达了。

到达宅邸後，母亲和伊莉斯，以及佣人们一同迎接了两人。
帕多利克向每个人都打了招呼，并且致以拥抱。
即便是佣人也一样。
虽说是下级贵族，不过鲁鲁也没见过帕多利克以外的贵族就是了，但他依然觉得帕多利克属於贵族中相当没有脾气的那种人了，无论村民还是佣人，都能坦率地与他们互动。
即便被直呼其名也不在乎，直接叫名字就好，甚至会这麼跟对方说。
如果哪裡出了差错的話，那个性格没准真的会被其他贵族问责。但是，因为村民们都会对此留神，而帕多利克在逗留於村子中的旅者或商人面前都会表现得相当正经，所以也就不会让人太过担心。
至于孩子们对这方面的注意则当然是不足的，似乎很让人担心。不过，也不会有什麼了不起的大人物来到这个村子，因此至今没弄出过什麼麻烦。
然後，帕多利克便先到自己的房间中，将随身的行李整理好。交给鲁鲁的那几本书，原先也被放在那裡面。
帕多利克总是会把书，当成是从王都返回时的礼物送给鲁鲁。
因为价格很高，所以这一带不怎麼能弄得到。好在帕多利克有着幾个喜欢藏书的朋友，於是就从他们那裡打听到了，如何能以相对便宜的价格入手书籍。
因为那个关係吗，帕多利克带回的书大多都很古旧，甚至其中缺页的也不在少数。即便如此，也依然能从中得到足够的信息，所以鲁鲁并不在意。
大家正一边围绕着準备午饭的母亲与伊莉斯，一边开始闲谈。
帕多利克的話题很丰富，主要是王都中發生的轶事。不过为了让鲁鲁这个年纪的人也能听懂，所以那个内容不会太过深入。但年龄与普通的孩子截然不同的鲁鲁，则对此感到了某种程度的信息不足。
而正是为了弥补这个，才决定和格兰（グラン）他们交流。不过，那个想法大概是成功了吧，鲁鲁现在正享受着，同父亲间久违的谈話。
父亲所说的事情都很有趣，但还是和冒险者的视点不太相同。虽然很好地传达了王都的氛围，但不太涉及与其他的国家间的关係如何如何之类的話题。
偶尔也夹杂着传闻之类的，粗略的内容。大概不是自己亲眼所见，而是从他人那裡听说，或是从书本上得来的吧。
对於那并不详尽的内容，因为鲁鲁的外表只有七岁，所以这也是无可奈何的事情吧。不过，无论什麼时候听，都会有一种遗憾的感觉挥之不去。
跟冒险者不同，对了，比如并不会涉及超过国境的事情。因为是对王国发誓忠诚的骑士，所以当然不会因为性格上的粗心大意，而去泄露机密的吧。
总体来说，父亲是那种与冒险者的随便马虎与轻鬆愉快截然相反的气质，经常会有人这麼评价。
吃饭的时候，帕多利克一边听着麦蒂娅要收养伊莉斯的話题，一边爽快地点了点头。
因为已经和伊莉斯谈过，所以能从气质之类的说不清的事情上，判断她没有问题吧。
文件之类的，决定在返回王都的时候提交。
接着，話题便转移到了有冒险者到村子裡来这件事上了。
那之中，鲁鲁跑去遗迹的事情，也被麦蒂娅捅了出来。但帕多利克并没有多麼生气，不要勉强就好，只是这麼说了就结束了。
然後，便是最重要的部分了。
鲁鲁静静地开口。
“那个……父亲，我有事想要和你商量。”

因为注意到了鲁鲁态度的改变，帕多利克也收起了那开朗的笑容，开始用认真的表情，注视着鲁鲁。
“什麼事？”

“我，十四岁的时候想要成为冒险者……所以，你能允许吗？”

看着没有任何掩饰，直接说出真意的鲁鲁，帕多利克露出了微笑。
“……也不是不行。我们家只是下级贵族，所以不会有太多的问题……实在不行的話，将来从亲戚那收养孩子，然後再让他继承家业就好。再说了，我和你母亲还都年轻，所以你会再多一个弟弟的可能性也是有的。只是啊……鲁鲁。”

没想到这麼简单就得到了承认。所以听到帕多利克的話後，鲁鲁并不明白他接下来要说些什麼。
於是，鲁鲁慢慢地点了点头，等待着後续的話语。
“啊。”

“冒险者啊，并不是嘴上说说就能做的那种简单工作。所以，你必须完成相应的课题。”

“课题，吗……”

究竟会提出什麼课题呢？
鲁鲁大致能猜到。
虽然父亲给人以温和平稳的印象，但骨子里仍是严厲的骑士。
这样的他所提出的课题。
已经不用再想了。
帕多利克说道。
“没错呢……在你十四岁的生日到来之前，你使剑的本领，必须得到我的认同。如果做不到的話，成为冒险者什麼的，还是趁早死心吧……你接受吗？”

说着那样的話，帕多利克的眼睛裡正闪烁着异样的光芒。那个表情也与刚才温柔的微笑截然不同，寄宿着强烈的意志。
曾经作为魔王，接受了无数战士挑战的鲁鲁，从帕多利克的眼中感受到了与他们别无二致的，那种有胜无败的气魄。
所以，鲁鲁答道。
不可能拒绝，无论是现在还是以前，鲁鲁明白这点。
“啊……我明白了。十四岁的生日之前，我会成为让父亲认同的剑士的。”

坦然地接受了帕多利克的条件，鲁鲁以毫无迷茫的表情断言了。於是，帕多利克再次露出了微笑。
但是，这个笑容跟刚才那平静的微笑不同……就像是战士终於找到了好对手时露出的相似。
“这才是我的兒子。那麼说了的話，就从明天开始特训吧。你会变强的，我相信着。”

说完後，父亲又变回了平时的表情。
结果，鲁鲁也不清楚能否胜过帕多利克。
帕多利克很强。
虽说不是这个国家中最强的。不过，那个身手却是货真价实的。
因为，他是这个国家最强最著名的皇家骑士团中，担任着剑术指导的人。
如果要说雷纳德（レナード）王国最强的話，通常而言，应该是皇家骑士团的团长，或者是宫廷魔术师长吧。
但是，只论剑术的話，这个国家中恐怕无人能出帕多利克之右。
在剑术方面，帕多利克有着旁人无法企及的天赋。
因此鲁鲁能够断言，帕多利克很强。
明白了以普通的觉悟是无法成为冒险者的，鲁鲁将吃完的餐具收拾好後，便拿出训练用的木刀，来到了宅邸的庭院。
从现在开始，每天都要进行特训。
如果不这麼做的話，战胜帕多利克什麼的，简直就是白日做梦吧。
鲁鲁如是想到。

