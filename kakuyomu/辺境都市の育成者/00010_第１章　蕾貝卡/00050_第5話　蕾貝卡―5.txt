世界树——是耸立於大陆中央的巨大树木。
不清楚到底有多高。
从使用飞行魔法上升到极限高度的魔法使那裡，传来了栖息着上位种（包含多数的真龙）的报告。
以及还没有看到树木的顶端。

虽然传闻树龄已达到幾千年甚至几万年，但是具体情况谁也不清楚。
这一带作为森妖精的圣域为此难以进入，要获取素材更是困难。
树枝制成魔法杖，树叶以及树叶制成回復药，其它还存在着现在所知的最高峰素材，多数的职人都以『生涯加工一次』作为梦想。

现在在我眼前的就是这样的东西，再加上赋予着7颗属性宝珠。
……要是看到了这种东西，就连发狂的职人也会蜂拥而至。

「怎麼露出一副奇怪的表情？我自认为是做的很不错的」
「……的确很漂亮。但是这个世界树和宝珠是真货吗？」
「是真的哦，拿在手裡瞧瞧吧」

春将魔法杖递给了我。
在拿在手裡的瞬间，我领悟到这是真货。
察觉到自己的魔力正活性化。不仅如此，就连平时无法使用的属性魔法也能够使用。
什麼？一脸笑眯眯的。

「给你提个有益的建议。不仅是火，也更多地使用雷更好呢。虽然看上去你很不擅长，不过你的适性更倾向雷哦」
「…………为什麼知道我的属性？」
「你看，毕竟我是培养者。看一眼就能明白了」

就算对方是艾露米娅，对於冒险者来说也不会将性命攸关的情报告诉他人。就算对方是公会职员也是如此。
的确我擅长火魔法，不擅长雷魔法。
但是为什麼他会知道？并且适性倾向雷？虽然无法突然就相信他……不过这种戏法。

「原因是这根魔法杖吧」
「唔……正确。拿着这根魔法杖魔力就会活性化，令人吃惊般的便利呢。所有属性都凑齐了，今後会更加活跃吧。話说回来你还真是聪明呢。基本上不管是哪个孩子在这裡都会拔出剑展开魔法」
「希望我拔剑吗？」
「呼呵，是在称讚你哦」
「不要捉弄我，差不多该告辞了」

这麼说着，将魔法杖递过去後站起身来。
要不要将内心中的想法——告诉他并且询问一下呢——不过将之抛在一旁。
……因为不管怎麼想都很可疑，实在是太可疑了。
刚才放在仓库裡的东西也好，这根魔法杖也好，都非比寻常。
越是交谈，常识就越是崩坏。

「明明吃了晚饭再回去也可以的，那傢伙也经常来蹭饭」
「不用了」
「当然也有甜食，除了草莓蛋糕还有其它的！」
「……不、不用了」
「是吗，那还真是遗憾。那样的話作为代替，给你第二个建议吧。如果想使用魔法剑的話，照现在这个样子是永远都做不到的哦」

瞬间拔剑挥出斩击。
不过——无法置信的是被魔力障壁所阻挡。刀刃完全无法更近一步。
一边抑制动摇的情绪询问他。

「……为什麼，你会知道这件事？」
「这是为什麼呢？」

柔和的笑容更让人感到火大。
魔法剑是将属性魔法赋予到武器上，从而使得攻击力获得飞跃提升的技能。
当习得这个技能之後，就能够对抗普通攻击无效的对手了。
真龙和恶魔这样的上位种通常展开着幾十层障壁。首先普通的攻击无效，就算突破了障壁身体自身也会装甲化。
於是长期和上位种战鬥的人编织出的就是这个魔法剑，这是上级前卫职业能够习得的鬥气术。
虽然或许现在还不需要，不过对於我的目标——到达第1阶位是必要的技能。
自从升格成为第8阶位之後，我就在努力习得魔法剑。
阅读各种文献，向已经习得能够信赖的熟练冒险者请求教诲。
但是还没有得到线索。
或许这个作为间接原因，其它技能也一筹莫展。
……明明谁也不知道这件事情的。

「你太逞强了。一口气登上阶梯的話，或许是会掉下去的哦。更何况习得魔法剑与鬥气术需要花上时间。为此轻视了其它技能努力白费了」
「……那样的話，你倒是说我要怎麼办才好！」
「正因为想要这个提示，所以今天才来到这裡不是吗？」
「…………」

也不能说没有这个想法。
虽然并没有相信传闻，不过只要能成为提示的話，不管是什麼都想要抓住。
不过……完全没有想到居然存在这样的人！
收回剑插入剑鞘裡，然後询问道。

「你想说如果我向你请教的話，我就能够成长吗？」
「当然。是呢，很快就会上升到第5阶位吧」
「……就算是开玩笑也要有限度」
「是吗？我认为只要努力的話，不久之後你就会到达第1阶位哦。在这之後就要看你自身了」

春说着没有任何根據的話，不过好像是认真的样子。
……这傢伙是怎麼回事。
为什麼对不久前才相遇的我给出这样的评价。
就连双亲都没有相信我的。

「嘛，不用想这麼多哦。总之明天早晨过来一趟吧，我也正好调整一下魔法杖」
「什麼意思？」

这麼询问之後，春的脸上浮现出柔和的笑容说道。

「明天开始和我组队，届时会告诉你许多事情。早饭的話就来这裡吃呢，尽情期待吧」
