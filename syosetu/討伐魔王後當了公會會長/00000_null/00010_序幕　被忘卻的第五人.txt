
「如果自己能够成为打败魔王的勇者的話」谁都曾经这样想过吧。

但是，那就像是要买彩票中头奖，機率接近零。（所以这就是你装逼的前奏？…）

为此，要比任何人都先到达魔王的宫殿，然後打倒魔王并全身而退。因为魔王的宫殿是一个很艰难的环境，在回来的路上死掉了，死後被人奉为英雄的人也有。

即使是平安归来，成为了打败魔王的勇者，那也就意味拥有超过魔王的力量，总会出现一些惧怕这份力量的人。

即使作为勇者被大家承认，获得了至上的荣耀，如果不好好处理人际关系，很容易进入修罗场。

那麼，怎麼办才好呢？我在讨伐魔王前就一直担心着这些问题。（(⊙_⊙)完全没考虑过失败的事…）

也许你会说我畏首畏尾的，难得加入了魔王讨伐队，当然得尽可能减少风险，并获得最多的回报啊。（嗯，谋士，所以你就这麼确定你上的车不会翻？！）

至上荣耀什麼的完全没兴趣。我就喜欢偷偷的避人眼目地把想要的东西搞到手。

为此，就是在讨伐魔王之前的战鬥中，我都贯彻了不当前锋，完全在後方支援。到打倒魔王为止，我都是这麼想的。

　◆◇◆

要打倒魔王，当然需要足够强大。简单来说，在冒险着公会被评定为SSS级就行了。

这个名叫格兰加姆的世界，在Arubein（厄尔本茵）王国，公会裡的冒险者的能力都会被严格评定。体力，魔力，技能，人脉关係等等，都是能力的组成部分，最後被当作“冒险者强度”数值化。并且，这个数值如果超过10万，能力级别就是SSS。

别说到达过这个级别的年轻人从没出现过了，王国歷史中，SSS级别的冒险者就没幾个。

但是，我们的世代却不同。应募了『通缉！魔王讨伐』为名贴出的委托，十多岁的少年少女，这些『奇迹之子』各自天赋异禀，他们进过磨练，纯粹的能力值就达到了公会标准中的SSS级。

他们奇迹般的5人，各自带有一个对应自己个性的外号。

『闪耀光剑・科迪』
『令人怜爱的灾厄・米拉露卡』
『沉默的镇魂者・忧玛』
『妖艳鬼神・爱玲』


最後是我――姑且强度超过了10万，但是毫无存在感的我被称作了『忘却的那个谁』。虽然想吐槽要起外号的話就多去调查打听，但还是算了。总之我的名字是迪库・席尔瓦。

然而，这样的强度超过10万的惊异的新人们，有一个明显的弱点。

那就是脑袋上的遗憾。全员因为天然呆同时又特别强，所以很不擅长制作作战计划。

科迪是无论面对怎样麻烦拥有特殊攻击技能的对手，都必须先报上姓名後才开战。其他的三位少女也是，尽管有着让普通男人都畏惧的力量，但各自都有着特殊的性格。最初组成魔王讨伐队的时候，队伍的机能完全處於瘫痪状态。

统帅这样的一个队伍，并分配好职责的人，是跟四人混的都比较熟的我。

为其他的四人出谋划策，活用他们的力量。也不能否认自己让其他四人战鬥，独占在後面OB。实际上，我近身战打不过科迪和爱玲，攻击魔法和辅助魔法也比不过米拉露卡和忧玛。所以这也算是各尽所长。我的能力值在『奇迹之子』中是最低的。

经过这样那样的困难，我指挥着我的同伴们，轻易就到达了魔王宫殿。

米拉露卡用自满的歼灭魔法攻击魔王，忧玛一边对付着魔王召唤出的死灵一边用镇魂魔法进行超度，爱玲变身成『鬼神』和魔王肉搏，最後科迪用光剑打倒了魔王。

我只给他们做出行动指示，上能力强化的buff，然後就是OB。大家每个人虽然战鬥力爆表，但是辅助能力全无。就连拥有镇魂之力的高级僧侣忧玛也没学过恢復和防御魔法。论攻击魔王也一样，正面吃上一击的話，就算是SSS级的冒险者也会丢手断脚的，如果没有我的强化魔法的話，估计会出现牺牲者。嘛，没有發生这样的事实在是万幸。

只能进行辅助导致自己的影响力太小让我有点在意，不过这也是我所期待的。處於魔王讨伐队末席的事实让我能取得一定程度上的功绩，但是我做出了什麼样的贡献却不想被太多人知道。到底是会当作『强化大家打辅助』的人多，还是认为只是默默在後方进行加油的人多呢？我就这麼考虑着。

「咕……能让我屈膝。人类啊，我就认同你们的这份力量吧」

「当真？ 如果保证以後再也不作坏的話，可以放你一马。为了不让你再次到人间作乱，绝不允许你走出魔王领地半步」

「领地就这样保留下来吗……？ 天真的男人。还是说这个已被魔物污染的土地已经不需要了」

「任谁都需要一个生存下去的地方。我是这麼想的」

科迪尽管死鬥之後浑身是血，却一脸爽朗的说出这种話。我倒是想吐槽你先止好血吧，但会回復魔法的只有我。本想在来魔王之城的途中拉一个专门负责回復的成员进来――现在说这也没办法。我咏唱了『治癒之光』之後，科迪瞬间就回復了。

「噗嗯人类哟，即使我现在屈服於你们，但是有光之地必有暗，之後肯定会有更强大的魔力再次颠覆人间世界」

「别管那些东西了。快点把打倒魔王的证物交过来，不然我们就剥光你，扛着你的身体回去了」

「噫！」

不愧被称作「怜爱的灾厄」，米拉露卡的威胁抱怨姿态萌人一脸。纯金黄色的头髮，大大的眼珠中带着坚定的意志。虽然只有11岁，比我小两岁，但她一点都吧介意这个――对自己的外貌和实力有着绝对的自信，年龄完全不是问题，这是她自己的話。

真是性格和外边一样苛刻同时毫不留情啊。黑色肤色的美女 -- 跟暗黑精灵拥有相似形态的魔王被强硬地剥掉衣服，魔王發出了悲鸣。

「本来很好奇魔王的灵魂是什麼样子的，像用一次镇魂试下的……有点遗憾呢」
「那、那是……已杀生为前提的啊。僧侣怎麼能说出这种話。」
「诶诶，为什麼？ 我可是想抚慰世上所有的灵魂的啊。包括迪库你的灵魂……」

忧玛咋一看上去像是一个同性恋末期患者，其实只是『镇魂恋癖』後期而已。这个天才僧侣才9岁，在我们的队伍中是最年幼的。如此年幼的她到底是被什麼怎麼样了才扭曲到了这个地步，我对这很是在意。尽管本人说过「僧侶都是这个样子」，但是我还是感觉有内情。

「喂，那该怎麼办？ 打倒魔王的证据……她头上戴着的项链是什麼？」

摆着等得不耐烦的表情，红髮的武斗家少女--爱玲这样说道。她是人和鬼族的混血，成长速度比人要快，所以才12岁就有着大人的成熟身材。自称「啵唷啵唷」的胸不比看起来20岁左右的魔王的逊色，我的少年心有时也会忍不住被诱惑住……这些先放到一边。
科迪看向魔王的脖子，找到了项链。然而他的脸慢慢染成红色，举动也变得奇怪起来。

「项、项链……迪库，还是你来取吧。」

「你在害怕什麼……姆……？」

魔王穿着捆缚式的衣服，胸以上的身体完全裸露着。而项链被藏在双峰之间。

科迪对女性完全没有免疫力--虽是这麼说，对於13岁的年龄来说，这也算情有可原，但是这也有点太过了。对我来说，因为怀有想热情地拥抱这对欧派的衝动，对於在双峰间探索项链这样的好事，当然是积极地自告奋勇。

「额……要、要夺走那个护符吗……就算没有了它，我的属性耐性，自动回復能力等都不会消失……同时普通的人类装备了它只会被诅咒，而且只是拿着就会被吸走生命力」

「喔喔……那真是……」

在魔王解说的时候，我默默的扯着带子--终於，金色的首饰被扯出来了。形状有点像魔法阵。虽然显得挺简朴的，但是發出的魔力却非比寻常。

「居然用那种H的眼光看待魔王，真差劲……你这个变态。变态迪库」
「啊哈哈，米拉露卡在吃醋呢。别担心啦，到时候会大起来的。跟我的一样♪」

「我、我才不在乎……」

米拉露卡看到我看向她时，狠狠的瞪了我一眼然後移开了视线。不说話的話挺可爱的，奈何她是个比男生还要强的性格--我已经完全习惯了。

比起这个，现在是护符的问题。还残留着上半身的魔王双峰的温度的项链，需要被带回去。

「反正只要不带上就不会被诅咒，魔王的护符，我们要拿走啦」

「呜……知，知道了。我如果老实地待这儿的話，能还给我么？」

「虽说保证不了，但是……5年後你来取回吧。如果那个时候你还遵守着我们定下的规则，就把护符还给你 。」

「……虽因你是小孩而轻视了你，却有着胆量和力量。年轻的勇者哦，你的名字叫什麼？」

「刚才也说过了，叫迪库。迪库。虽说忘记也没关係，但是还是尽量记住吧。不然还不了你护身符的。」

我取走魔王的护符--虽说普通人会被诅咒，但是凭我的实力的話，可以把这个力量抑制住。虽说想着会不会带上这个项链会不会成为魔王，但是这并不是我的目的。

-- 说起来，有一句話忘了说。

「还有，我不是勇者。勇者是这个科迪。我只是跟大家混助攻的」

「……不，不是那样的……为什麼，要谦逊到这个地步。怎麼看这一行人的中心是……」

我没有理会魔王最後的話，直接走出了魔王城。生存下来的魔物们看着我们，都不敢出手阻止我们。



